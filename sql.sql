--Tworzenie bazy danych (kod z pgAdmin nie wiem czy Reichel wątów nie będzie miał)
CREATE DATABASE "Baza_filmow"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
    
--Tworzenie tablicy z filmami (kod z pgAdmin part 2 nie wiem czy Reichel wątów nie będzie miał)   
CREATE TABLE public.movies
(
    id_user integer NOT NULL,
    id_movie bigint NOT NULL DEFAULT nextval('seq'::regclass),
    name character(100) COLLATE pg_catalog."default" NOT NULL,
    opis character(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT movies_pkey PRIMARY KEY (id_movie)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.movies
    OWNER to postgres;
    
--Tworzenie tablicy z użytkownikami  (kod z pgAdmin part 3 nie wiem czy Reichel wątów nie będzie miał)
CREATE TABLE public.users
(
    id integer NOT NULL,
    username character(50) COLLATE pg_catalog."default" NOT NULL,
    password character(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;
  
-- Dodanie użytkownika z hasłem haszownym     
INSERT INTO users (id, username, password) VALUES ('1', 'admin', MD5('admin'))