<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Strona główna</title>
</head>
<body>
<form action="admin.php" method="post">
    <input type="submit" class="btn btn-link" value="Panel administracyjny">
</form>
<div class="jumbotron">
    <h1 style="text-align: center;">Baza filmów</h1>
</div>

<div class="container">
    <h2>Lista filmów</h2>
    <ul class="list-group">
        <li class="list-group-item">
            <?php
            $conn = pg_connect("host=localhost port=5433 dbname=Baza_filmow user=postgres password=admin");

                $result = pg_query($conn, "SELECT * FROM movies ORDER BY name ASC");

            while ($row = pg_fetch_array($result)) {?>

                            <h3><?php echo $row['name'];?></h3>
                <p class="font-italic"><?php echo $row['opis'];?></p>
                <hr>

            <?php } ?>
        </li>
    </ul>
</body>
</html>
