<?php
    include ('config.php');
if (!isset($_SESSION['id'])) {
        header('location: login.php');
    }?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Panel administracyjny</title>
</head>
<body>
<form action="config.php" method="post" id="logout" >
    <input type="submit" class="btn btn-link" name="logout" value="Wyloguj">
</form>
<div class="jumbotron">
    <h1 style="text-align: center;">Panel administracyjny</h1>
</div>
<div class="container">
    <ul class="list-group">
        <li class="list-group-item">
            <h3>Dodaj nowy film: </h3>
<form method="post" action="config.php">
    <input class="form-control" name="namemovie" placeholder="nazwa filmu">
    <textarea name="description" class="form-control" rows="5" id="comment" placeholder="opis"></textarea>
    <input type= "submit" class="btn btn-success" name = "create" value="Dodaj">
</form>
        </li></ul>
</div>
<br>
<div class="container">
<h2>Lista filmów</h2>
        <label>Sortowanie po nazwach filmow:</label>
        <div class="btn-group btn-group-xs">
            <form action="config.php" method="post" id="a" >
            <input type="submit" class="btn btn-link" name="a" value="od A do Z">
            </form>
            <form action="config.php" method="post" id="z" >
            <input type="submit" class="btn btn-link" name="z" value="od Z do A">
            </form>
        </div>
    <ul class="list-group">
        <li class="list-group-item">
    <?php
    $iduser = $_SESSION["id"];
    $test=5;
    $conn = pg_connect("host=localhost port=5433 dbname=Baza_filmow user=postgres password=admin");
    $getsort = $_COOKIE['Cookie'];
    if($getsort==1){
    $movielist = pg_query($conn, "SELECT * FROM movies WHERE id_user='$iduser' ORDER BY name ASC");}
    else{$movielist = pg_query($conn, "SELECT * FROM movies WHERE id_user='$iduser' ORDER BY name DESC");}
    while ($row = pg_fetch_array($movielist)) {?>
<form action="config.php" method="post" id="edit" >
<table>
    <tr>
        <td><input class="form-control" name="name" value="<?php echo $row['name'];?>" type="text" class="input-field"></td>
        <td><input class="form-control" name="description" value="<?php echo $row['opis'];?>" type="text" class="input-field"></td>
        <td><input class="form-control" name="id" style="display: none;" value="<?php echo $row['id_movie'];?>"></td>
        <td><input type="submit" class="btn btn-primary" name="save" value="Zapisz zmiany"></td>
        <td><input type="submit" class="btn btn-danger" name="delate" value="Usuń"></td>
    </tr>
</table>
</form>
    <?php } ?>
        </li>
    </ul>
</body>
</html>
